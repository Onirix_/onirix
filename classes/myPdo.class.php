<?php

require_once(ABSPATH.'/server_config/bdd_access.php');

class MyPdo {

    /**
     * @var mixed
     */
    private static $instance = null;

    // private function __construct() {
    //     return new PDO(PDODSN, PDOUSERNAME, PDOPASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"'));
    // }

    /**
     *
     */
    public static function getInstance() {
        if (is_null(self::$instance) === true) {
            self::$instance = new PDO(PDODSN, PDOUSERNAME, PDOPASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "UTF8"'));
        }
        return self::$instance;
    }
}
