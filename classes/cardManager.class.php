<?php

require_once(CLASSESPATH.'myPdo.class.php');
require_once(CLASSESPATH.'manager.class.php');
require_once(CLASSESPATH.'card.class.php');


/**
 * Classe abstraite pour gérer les accès BDD des cartes
 */
abstract class CardManager extends Manager {

    /**
     * Récupère
     * @param int $factionId
     *
     * @return array
     */
    public function getCardsRef( int $factionId ): array {
        $str =
            'SELECT  * FROM `cardsref`
            WHERE `id_faction` = :factionId';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'factionId', $factionId ) ) {
                    if ( $sql->execute() ) {
                    $cards = [];
                    while ( $dataCard = $sql->fetch( PDO::FETCH_ASSOC ) ) {
                        $card = new Card( $dataCard['name'], $dataCard['description'], +$dataCard['type'], +$dataCard['cost'], +$dataCard['pv'], +$dataCard['pf'], $dataCard['spell'], +$dataCard['id_faction'],  $dataCard['image'] );
                        $card->setIdRef( $dataCard['id'] );
                        $cards[] = $card;
                    }
                    return $cards;
                }
            }
        }
        return [];
    }

    /**
     * @param array $cards
     * @param int   $idPlayer
     */
    public function registerCards( array $cards, int $idPlayer) {
        // NOTE: No bind value here, because trop galère ...
        $str = 'INSERT INTO `'.DBPREFIX.'cards` (`id_player`, `id_cardref`, `state`, `pv`, `deck_order`) VALUES ';
        foreach ($cards as $card) {
            $str .= '(\'' . $idPlayer . '\', \'' . $card->getIdRef() . '\', \'1\', \'' . $card->getPV() . '\', \'0\'  ), ';
        }
        $str = substr( $str, 0, -2).';';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            $sql->execute();
        }
    }

    public function loadCards( int $idPlayer ) {
        $str='
            SELECT `'.DBPREFIX.'cards`.`id` as `id`, `cardsref`.`name` as `name`, `cardsref`.`description` as `description`, `cardsref`.`type` as `type`, `cardsref`.`cost` as `cost`, `'.DBPREFIX.'cards`.`pv` as `pv`, `cardsref`.`pf` as `pf`, `cardsref`.`spell` as `spell`, `cardsref`.`id_faction` as `faction`, `cardsref`.`image` as `image`, `'.DBPREFIX.'cards`.`deck_order` as `deckOrder`
            FROM `'.DBPREFIX.'cards`
            INNER JOIN `cardsref` ON `'.DBPREFIX.'cards`.`id_cardref` = `cardsref`.`id`
            WHERE `'.DBPREFIX.'cards`.`id_player` = :idPlayer
            ORDER BY `deck_order`,`id` DESC
            ';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'idPlayer', $idPlayer ) ) {
                    if ( $sql->execute() ) {
                    $cards = [];
                    while ( $dataCard = $sql->fetch( PDO::FETCH_ASSOC ) ) {
                        $card = new Card( $dataCard['name'], $dataCard['description'], +$dataCard['type'], +$dataCard['cost'], +$dataCard['pv'], +$dataCard['pf'], $dataCard['spell'], +$dataCard['faction'], $dataCard['image'] );
                        $card->setId( $dataCard['id'] );
                        $cards[] = $card;
                    }
                    return $cards;
                }
            }
        }
        return [];
    }

    public function updateCards( array $cards, int $playerId ) {
        foreach ( $cards as $card ) {
            if ( ! self::updateOneCard( $card, $playerId ) ) {
                throw new \Exception("Erreur update cards", 1);
            }
        }
    }

    /**
     * @param Card $card
     * @param int  $playerId
     *
     * @return bool
     */
    public function updateOneCard( Card $card, int $playerId ): bool {
        // if ( $card->getUpdated() ) {
        //     return true;
        // }
        $str = '
                    UPDATE `'.DBPREFIX.'cards`
                    SET `state` = :state, `pv` = :pv, `deck_order` = :deckOrder
                    WHERE `'.DBPREFIX.'cards`.`id` = :idCard
                ';
            if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
                if ( $sql->bindValue( 'state', $card->getState() ) && $sql->bindValue( 'pv', $card->getPv() ) && $sql->bindValue( 'deckOrder', $card->getDeckOrder() ) && $sql->bindValue( 'idCard', $card->getId() ) ) {
                    if ( $sql->execute() ) {
                        $sql->closeCursor();
                        $card->setUpdated( true );
                        return true;
                    } else {
                        echo 'error execute card';
                        return false;
                    }
                } else {
                    echo 'error bind card';
                    return false;
                }
            } else {
                echo 'error prepare card';
                return false;
            }
        }

    }
