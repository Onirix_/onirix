<?php

require_once(CLASSESPATH.'sets.class.php');


/**
 * L'incarnation d'un user dans une partie donnée
 * Possède des SetCards composés de Card
 */
class Player {
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $pv;
    /**
     * @var int
     */
    private $pm;
    /**
     * '1 = en attente d\'adversaire, 2 = en cours, 3 = finie, 0 = annulée',
     * @var int
     */
    private $state;
    /**
     * @var int
     */
    private $gameId;
    /**
     * @var string
     */
    private $pseudo;
    /**
     * @var int
     */
    private $factionId;
    /**
     * @var bool
     */
    private $loaded;
    /**
     * @var bool
     */
    private $updated;
    /**
     * @var Deck
     */
    private $deck;
    /**
     * @var Hand
     */
    private $hand;
    /**
     * @var Rest
     */
    private $rest;
    /**
     * @var Board
     */
    private $board;
    /**
     * @var Discard
     */
    private $discard;


    /**
     *
     */
    public function __construct() {
        $this->setLoaded( false )->setUpdated( false );
        $this->deck = new Deck();
        $this->hand = new Hand();
        $this->rest = new Rest();
        $this->board = new Board();
        $this->discard = new Discard();
    }

    /**
     * @param array $datas
     *
     * @return bool
     */
    public function load(array $datas): bool {
        if (
            isset($datas['id']) && is_numeric($datas['id']) && +$datas['id'] > 0 &&
            isset($datas['id_faction']) && is_numeric($datas['id_faction']) && +$datas['id_faction'] > 0 &&
            isset($datas['id_game']) && is_numeric($datas['id_game']) && +$datas['id_game'] > 0 &&
            isset($datas['pv']) && is_numeric($datas['pv'])  &&
            isset($datas['pm']) && is_numeric($datas['pm'])  &&
            isset($datas['state']) && is_numeric($datas['state']) && +$datas['state'] > 0 &&
            isset($datas['pseudo'])
        ) {
            $this
                ->setId( +$datas['id'] )
                ->setFactionId( +$datas['id_faction'] )
                ->setGameId( +$datas['id_game'] )
                ->setPv( +$datas['pv'] )
                ->setPm( +$datas['pm'] )
                ->setState( +$datas['state'] )
                ->setPseudo( $datas['pseudo'] )
                ->setLoaded( true )
                ->setUpdated( true );
                PlayerManager::loadCards( $this );
                return true;
            }
            return false;
    }

    //-----------------------------------------------------
    // Getters
    //-----------------------------------------------------

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPv(): int {
        return $this->pv;
    }

    /**
     * @return int
     */
    public function getPm(): int {
        return $this->pm;
    }

    /**
     * @return int
     */
    public function getState(): int {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getGameId(): int {
        return $this->gameId;
    }

    /**
     * @return string
     */
    public function getPseudo(): string {
        return $this->pseudo;
    }

    /**
     * @return int
     */
    public function getFactionId(): int {
        return $this->factionId;
    }

    /**
     * @return bool
     */
    public function getLoaded(): bool {
        return $this->loaded;
    }

    /**
     * @return bool
     */
    public function getUpdated(): bool {
        return $this->updated;
    }

    /**
     * @param int $id
     *
     * @return Card
     */
    public function getCard( int $id ): Card {
        //vérifier si isset ?
        return $this->getCards()[$id];
    }

    /**
     * @return array
     * Retourne toutes les cartes du joueur, on utilise l'opérateur d'union + pour conserver les indexes
     */
    public function getCards(): array {
        return $this->deck->getCards() + $this->hand->getCards() + $this->rest->getCards() + $this->board->getCards() + $this->discard->getCards();
    }

    /**
     * @return int nombre de cartes dans le deck
     */
    public function getDeckSize() {
        return count($this->deck->getCards());       // Taille de ma pioche
    }

    /**
     * @return array[Card] toutes les cartes du deck
     */
    public function getDeck() {
        return $this->deck->getCards();          // Mon plateau
    }

    /**
     * @return Discard
     */
    public function getDiscard(): Discard {
        return $this->discard;
    }

    /**
     * @return int nombre de cartes dans la défausse
     */
    public function getDiscardSize() {
        return count( $this->discard->getCards() );    // Taille de ma
    }

    /**
     * @return array[Card] toutes les cartes du board
     */
    public function getBoard() {
        return $this->board->getCards();          // Mon plateau
    }

    /**
     * @return int nombre de cartes dans le board
     */
    public function getBoardSize() {
        return count( $this->board->getCards() );    // Taille de ma
    }

    /**
     * @return array[Card] toutes les cartes du rest
     */
    public function getRest() {
        return $this->rest->getCards();          // Ma zone de repos
    }

    /**
     * @return int nombre de cartes dans le rest
     */
    public function getRestSize() {
        return count( $this->rest->getCards() );
    }

    /**
     * @return array[Card] toutes les cartes de la main
     */
    public function getHand() {
        return $this->hand->getCards();          // Ma main
    }

    /**
     * @return int nombre de cartes dans la main
     */
    public function getHandSize() {
        return count( $this->hand->getCards() );    // La taille de la main
    }



    //-----------------------------------------------------
    // Setters
    //-----------------------------------------------------

    /**
     * @param int $id
     *
     * @return static
     */
    public function setId(int $id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int $pv
     *
     * @return static
     */
    public function setPv(int $pv) {
        $this->pv = $pv;
        return $this;
    }

    /**
     * @param int $pm
     *
     * @return static
     */
    public function setPm(int $pm) {
        $this->pm = $pm;
        return $this;
    }

    /**
     * @param int $state
     *
     * @return static
     */
    public function setState(int $state) {
        $this->state = $state;
        return $this;
    }

    /**
     * @param int $gameId
     *
     * @return static
     */
    public function setGameId(int $gameId) {
        $this->gameId = $gameId;
        return $this;
    }

    /**
     * @param string $pseudo
     *
     * @return static
     */
    public function setPseudo(string $pseudo) {
        $this->pseudo = $pseudo;
        return $this;
    }

    /**
     * @param bool $loaded
     *
     * @return static
     */
    public function setLoaded( bool $loaded ) {
        $this->loaded = $loaded;
        return $this;
    }

    /**
     * @param bool $updated
     *
     * @return static
     */
    public function setUpdated( bool $updated ) {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @param int $factionId
     *
     * @return static
     */
    public function setFactionId( int $factionId ) {
        $this->factionId = $factionId;
        return $this;
    }



    //==================================================
    // METHODS
    //==================================================


    /**
     * Réalise la fin de tour du joueur : met toutes les cartes 'rest' dans 'board'
     */
    function endTurn() {
        if ( $this->getRestSize() > 0) {          //si le rest est vide, on ne fait rien ici
            $this->moveCards( $this->getRest(), 'Board' );
        }
        $this->setPm( 0 )->setUpdated( false );
    }

    /**
     * @param int $cardId
     * @return false|string retourne false si erreur, sinon le nom de l'effet de la carte (le sort, ou rien si carte classique)
     * la méthode met la carte en jeu (si créature) ou en défausse (si sort)
     */
    public function playCard( int $cardId ) {
        if ( $this->getCard( $cardId ) === null || $this->getCard( $cardId )->getCost() > $this->pm ) {
            return false;
        }
        $card = $this->getCard( $cardId );
        if ($card->getType() === Card::SPELL) {           //les sorts vont direct dans la défausse
            $this->moveCards( $card, 'Discard' );
        } else {
            $this->moveCards( $card, 'Rest' );    //alors que les créatures vont dans le rest
        }
        $this->spendMana( $card->getCost() );
        $this->setUpdated( false );
        return $card->play();
    }


    /**
     * @param Card[]|Card $cards : on peut ne mettre qu'une carte, ou un tableau
     * // TODO: pouvoir directement mettre l'id d'une carte et laisser le système se débrouiller
     * @param string $to le nom d'un set : deck, hand, rest, board, discard
     * @return bool : succès ou échec
     */

    public function moveCards($cards, string $to): bool {
        if (is_a($cards, 'Card')) {         //pour si on fourni juste une carte ;)
            $cards = [$cards];
        }
        $this->updated = false;         // NOTE: vérif à faire ?
        switch ($to) {
            case 'Deck':
                $this->deck->addCards($cards);
                return true;
            case 'Hand':
                $this->hand->addCards($cards);
                return true;
            case 'Rest':
                $this->rest->addCards($cards);
                return true;
            case 'Board':
                $this->board->addCards($cards);
                return true;
            case 'Discard':
                $this->discard->addCards($cards);
                return true;
            default:
                return false;
        }
    }

    /**
     * @return bool succès ou échec
     * Vérifie si le jeu est terminé (du point de vue de CE joueur) : plus de PV ou plus de cartes en pioche/main/jeu
     */
    public function checkEndGame(): bool {
        // var_dump( $this->getDeckSize() == 0 && $this->getHandSize() == 0 && $this->getRestSize() == 0 && $this->getBoardSize() == 0 );
        if ($this->pv < 1 || ($this->getDeckSize() == 0 && $this->getHandSize() == 0 && $this->getRestSize() == 0 && $this->getBoardSize() == 0 )) {    // TODO: se servir d'un getter de carte plus riche
            $this->state = 3;
            $this->updated = false;
            return true;
        } else {
            return false;
        }
    }


    // /**
    //  * @return bool succès ou échec
    //  * met à jour le joueur dans la base de donnée (si besoin), ainsi que ses sets de cartes (qui eux-mêmes mettront les cartes à jour)
    //  */
    // public function updateDB(): bool {
    //     if (!$this->updated) {
    //         $str = '
    //             UPDATE `'.DBPREFIX.'players`
    //             SET `pv` = :pv, `pm` = :pm, `state` = :state
    //             WHERE `'.DBPREFIX.'players`.`id` = :idPlayer
    //         ';
    //         if ($sql = $this->pdo->prepare($str)) {
    //             if ($sql->bindValue('pv', $this->pv) && $sql->bindValue('pm', $this->pm) && $sql->bindValue('idPlayer', $this->id)  && $sql->bindValue('state', $this->state)) {
    //                 if ($sql->execute()) {
    //                     $this->updated = true;
    //                 } else {
    //                     echo 'error execute player';
    //                     return false;
    //                 }
    //             } else {
    //                 echo 'error bind player';
    //                 return false;
    //             }
    //         } else {
    //             echo 'error prepare player';
    //             return false;
    //         }
    //     }
    //     foreach ($this->getSets() as $set) {
    //         $set->updateDB();
    //     }
    //     return true;
    // }

    /**
    * @param int $numberCards : le nombre de cartes à piocher
    * Déplace des cartes de la pioche vers la main (en vérifiant qu'on ne pioche pas plus de cartes que ce qu'on en possède !)
    */
    public function draw(int $numberCards) {
        $numberCards = min($numberCards, $this->getDeckSize());         //pour ne pas piocher plus de cartes qu'on n'en possède
        //todo if $numberCards > 0
        $this->moveCards($this->deck->drawCards($numberCards), 'Hand');
        // TODO: Faire une entrée dans la table des draws !!!! (peut être via un return $draw, et laisser game gérer ...)
        $this->updated = false;
    }

    /**
    * // TODO: fonction mélange deck pas faite
    */
    public function shuffleDeck() {
        // TODO: if count(deck) > 1
        $this->updated = false;
    }

    /**
    * @return bool
    * Est-ce que le joueur a au moins un bouclier en jeu
    */
    public function hasShield(): bool {
        foreach ($this->getBoard() + $this->getRest() as $creature) {
            if ($creature->isShield()) {
                return true;
            }
        }
        return false;
    }



    /**
     * @param int $heal
     * soigne le joueur NOTE : pas de maximum ici, à voir
     */
    public function heal(int $heal) {
        // TODO: if heal > 0
        $this->pv += $heal;               //Peut-on dépasser 20 ? à déterminer
        $this->updated = false;
    }

    /**
     * @param int $deg
     * NOTE : peut descendre en dessous de 0 pv ...
     */
    public function hurt(int $deg) {
        // TODO: if hurt > 0
        $this->pv -= $deg;
        $this->updated = false;
    }

    /**
     * @param int $mana
     * NOTE : pas de gestion du mana négatif ...
     */
    public function spendMana(int $mana) {
        // TODO: if $mana > 0
        $this->pm -= $mana;
        $this->updated = false;
    }

    //
    // /**
    //  * @return array[SetCards]
    //  * pas super utile une fois qu'on aura mis des accesseurs plus modernes (concat de toutes les cartes par exemple)
    //  */
    // public function getSets() {
    //     return [$this->deck, $this->hand, $this->rest, $this->board, $this->discard];
    // }






}
