<?php

// TODO: finir get/set sur cards en gérant l'héritage, parceque là j'ai tout cassé !!!

require_once(CLASSESPATH.'card.class.php');

/**
 * Un SetCards est un ensemble de carte
 * state : '1 = deck, 2 = hand, 3 = rest, 4 = board, 5 = discard'
 */
class SetCards implements Countable, ArrayAccess {
    /**
     * @var array
     */
    private $cards = [];
    /**
     * @var bool
     */
    private $updated = false;

    //
    // /**
    //  * @param int $idPlayer : charge le set à partir de l'id du player, va chercher toutes les cartes et crée les objets carte
    //  * @return bool succès ou échec
    //  * Charge les cartes depuis la base de donnée
    //  * On fait ça set par set car on peux imaginer par la suite ne pas toujours tout charger (exemple : ne pas charger la pioche lors de la résolution d'une attaque de créature)
    //  */
    // function load(int $idPlayer): bool {
    //     $str='
    //         SELECT `'.DBPREFIX.'cards`.`id` as `id`, `cardsref`.`name` as `name`, `cardsref`.`description` as `description`, `cardsref`.`type` as `type`, `cardsref`.`cost` as `cost`, `'.DBPREFIX.'cards`.`pv` as `pv`, `cardsref`.`pf` as `pf`, `cardsref`.`spell` as `spell`, `cardsref`.`id_faction` as `faction`, `'.DBPREFIX.'cards`.`deck_order` as `deckOrder`
    //         FROM `'.DBPREFIX.'cards`
    //         INNER JOIN `cardsref` ON `'.DBPREFIX.'cards`.`id_cardref` = `cardsref`.`id`
    //         WHERE `'.DBPREFIX.'cards`.`state` = :state AND `'.DBPREFIX.'cards`.`id_player` = :idPlayer
    //         ORDER BY `deck_order`,`id` DESC
    //         ';
    //     if ($sql = $this->pdo->prepare($str)) {
    //         if ($sql->bindValue('state', static::STATE) && $sql->bindValue('idPlayer', $idPlayer)) {
    //             if ($sql->execute()) {
    //                 $this->cards = [];
    //                 //fetchAll
    //                 while ($dataCard = $sql->fetch(PDO::FETCH_ASSOC)) {
    //                     $this->cards[+$dataCard['id']] = new Card(+$dataCard['id'], $dataCard['name'], $dataCard['description'], +$dataCard['type'], +$dataCard['cost'], +$dataCard['pv'], +$dataCard['pf'], $dataCard['spell'], +$dataCard['faction'], static::STATE, +$dataCard['deckOrder'], $this->pdo);        //$idC, $name, $description, $type, $cost, $pf, $spell, $faction, $state, $deckOrder, $pdo
    //                 }
    //                 //prepare, conditionnel à faire
    //                 $sql->closeCursor();
    //                 return ($this->updated = true);
    //             } else {
    //                 //echo 'error execute set';
    //                 return false;
    //             }
    //         } else {
    //             //echo 'error prepare set';
    //             return false;
    //         }
    //     } else {
    //         //echo 'error prepare set';
    //         return false;
    //     }
    // }


    //==================================================
    // IMPLEMENTS Countable
    //==================================================

    /**
     * @inheritDoc
     */
    public function count(): int {
        return count( $this->getCards() );
    }

    //==================================================
    // IMPLEMENTS ArrayAccess
    //==================================================

    /**
     * @inheritDoc
     */
    public function offsetSet( $offset, $value ) {
        if ( is_null( $offset ) ) {
            $this->cards[] = $value;
        } else {
            $this->cards[$offset] = $value;
        }
        $this->setUpdated( false );
    }

    /**
     * @inheritDoc
     */
    public function offsetExists( $offset ) {
        foreach ( $this->cards as $key => $value ) {
            if ( $value->getId() === $offset ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset( $offset ) {
        unset( $this->cards[$offset] );
        $this->setUpdated( false );
    }

    /**
     * @inheritDoc
     */
    public function offsetGet( $offset ) {
        foreach ( $this->getCards() as $key => $value ) {
            if ( $value->getId() == $offset ) {
                return $value;
            }
        }
        return null;
        //return isset($this->cards[$offset]) ? $this->cards[$offset] : null;
    }

    //-----------------------------------------------------
    // Getters
    //-----------------------------------------------------

    /**
     * @return Card[] : tableau de cartes, avec en index leur id
     */
    public function getCards(): array {
        return $this->cards;
    }


    //-----------------------------------------------------
    // Setters
    //-----------------------------------------------------

    /**
     * @param bool $updated
     *
     * @return static
     */
    public function setUpdated( bool $updated ) {
        $this->updated = $updated;
        return $this;
    }

    //==================================================
    // METHODS
    //==================================================

    /**
     * @param int $n : nombre de cartes qu'on désire piocher, par défaut 1
     * @return Card[] : tableau de cartes piochées
     */
    public function drawCards( int $n = 1 ): array {
        $cards = [];
        for ( $i=0; $i < $n; $i++ ) {
            $cards[] = array_pop( $this->cards );
            // $cards[$i]->setState( 0 );          //pas nécessaire ici, car la carte piochée va bien quelque part ensuite
        }
        $this->setUpdated( false );
        return $cards;
    }

    /**
     * @param Card[]|Card $arrayCards : peut être un tableau avec une ou des cartes, ou seulement une carte
     * Ajoute un ou des cartes au set (dans le cas du deck, on l'ajoute à la fin pour le moment)
     */
    public function addCards( $arrayCards ) {
        if ( is_a( $arrayCards, 'Card' ) ) {
            $arrayCards = [$arrayCards];
        }
        foreach ( $arrayCards as $key => $card ) {
            $card->setState( static::STATE );
            if ( static::STATE === 1 ) {           //si pioche
                $arrayCards[$key]->setDeckorder( $this->getMaxDeckorder() + 1 );   // NOTE: modifier pour ajouter au sommet de la pioche ? à voir
            } else {
                $arrayCards[$key]->setDeckorder( 0 );   //
            }
            $this->cards[$card->getId()] = $arrayCards[$key];
        }
        $this->setUpdated( false );
    }

    // /**
    //  * @return bool succès // TODO: vérifier qu'aucun problème côté updateDB des cartes
    //  */
    // public function updateDB(): bool {
    //     foreach ($this->getCards() as $card) {
    //         $card->updateDB();
    //     }
    //     return ($this->updated = true);
    // }

}

class Deck extends SetCards {
    /**
     * @var int
     */
    const STATE = 1;

    /**
     * @return int le deckOrder maximal du deck
     */
    function getMaxDeckorder(): int {
        $max = 0;
        foreach ( $this->getCards() as $card ) {
            if ( $card->getDeckorder() > $max ) {
                $max = $card->getDeckorder();
            }
        }
        return $max;
    }

}

class Hand extends SetCards {
    /**
     * @var int
     */
    const STATE = 2;
}

class Rest extends SetCards {
    /**
    * @var int
    */
    const STATE = 3;
}

class Board extends SetCards {
    /**
    * @var int
    */
    const STATE = 4;
}

class Discard extends SetCards {
    /**
     * @var int
     */
    const STATE = 5;
}
