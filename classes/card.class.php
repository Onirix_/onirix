<?php

/**
 * Classe carte, rassemblées dans des sets de cartes. Chaque carte sait dans quel set elle est grâce à l'attribut state
 */
class Card {
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $idRef;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $description;
    /**
     * @var int : '1 : creature / 2 : bouclier / 3 : sort'
     */
    private $type;
    /**
     * @var int
     */
    private $cost;
    /**
     * @var int
     */
    private $pv;
    /**
     * @var int
     */
    private $pf;
    /**
     * @var null|string
     */
    private $spell;
    /**
     * @var int
     */
    private $faction;
    /**
     * @var int
     */
    private $state;
    /**
     * @var int : '1 = deck, 2 = hand, 3 = rest, 4 = board, 5 = discard'
     */
    private $deckOrder;
    /**
     * @var bool
     */
    private $updated;
    /**
     * @var string
     */
    private $image;

    /**
     * @var int
     */
    public const CREATURE = 1;
    /**
     * @var int
     */
    public const SHIELD = 2;
    /**
     * @var int
     */
    public const SPELL = 3;

    /**
     * @param int $idC
     * @param string $name
     * @param string $description
     * @param int $type : '1 : creature / 2 : bouclier / 3 : sort'
     * @param int $cost
     * @param int $pv
     * @param int $pf
     * @param string|null $spell        // TODO: à vérifier pour le null
     * @param int $faction
     * @param string $image
     * @param int $state : '1 = deck, 2 = hand, 3 = rest, 4 = board, 5 = discard'
     * @param int $deckOrder
     */
    function __construct( string $name, string $description, int $type, int $cost, int $pv, int $pf, $spell, int $faction, string $image, int $state = 0, int $deckOrder = 0) {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;          // 1 : creature / 2 : bouclier / 3 : sort
        $this->cost = $cost;
        $this->pv = $pv;
        $this->pf = $pf;
        $this->spell = $spell;
        $this->faction = $faction;
        $this->setImage( $image );
        $this->state = $state;
        $this->deckOrder = $deckOrder;
    }

    /**
     * @return string
     *      // TODO: exploiter __toString des cartes pour faire un affichage d'une carte en jeu
     */
    public function __toString(): string {                              //la fonction qui est appellée lorsqu'on fait un echo de l'objet (je sais pas si le public est nécessaire)
        return $this->name.'('.$this->pv.'pv)';
    }

    // /**
    //  * @return bool
    //  * Met à jour la carte dans la BDD, ssi elle a subit une modification récente ($this->updated == true)
    //  */
    // public function updateDB(): bool {
    //     if (!$this->updated) {
    //         $str = '
    //             UPDATE `'.DBPREFIX.'cards`
    //             SET `state` = :state, `pv` = :pv, `deck_order` = :deckOrder
    //             WHERE `'.DBPREFIX.'cards`.`id` = :idCard
    //         ';
    //         if (($sql = MyPdo::getInstance()->prepare($str)) !== false) {
    //             if ($sql->bindValue('state', $this->state) && $sql->bindValue('pv', $this->pv) && $sql->bindValue('deckOrder', $this->deckOrder) && $sql->bindValue('idCard', $this->id)) {
    //                 if ($sql->execute()) {
    //                     $sql->closeCursor();
    //                     return ($this->updated = true);
    //                 } else {
    //                     echo 'error execute card';
    //                     return false;
    //                 }
    //             } else {
    //                 echo 'error bind card';
    //                 return false;
    //             }
    //         } else {
    //             echo 'error prepare card';
    //             return false;
    //         }
    //     } else {
    //         return false;
    //     }
    // }

    //==================================================
    // SETTERS
    //==================================================

    /**
     * @param int $id
     *
     * @return static
     */
    public function setId(int $id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int $state : '1 = deck, 2 = hand, 3 = rest, 4 = board, 5 = discard'
     * @return bool true s'il a été changé, false sinon
     */
    public function setState(int $state): bool {
        if ($this->state !== $state) {
            $this->state = $state;
            return !($this->updated = false);
        } else {
            return false;
        }
    }

    /**
     * @param int $value : la position dans la pioche // NOTE: aucune vérification que la position ne soit pas déjà prise par une autre carte
     * @return bool true s'il a été changé, false sinon
     */
    public function setDeckorder(int $value): bool {
        $this->deckOrder = $value;
        if ($this->deckOrder !== $value) {
            $this->deckOrder = $value;
            return !($this->updated = false);
        } else {
            return false;
        }
    }

    /**
     * @param int $deg : nombre de dégâts infligés
     * Blesse la carte de $deg points.
     * Si ses points de vies tombent à zéro (NOTE: ou moins, pas de contrôle du négatif pour le moment), on change son state (mais on ne la déplace pas, ça sera fait automatiquement à l'update)
     */
    public function hurt(int $deg) {
        $this->pv -= $deg;               //Peut-on descendre en dessous de 0 ? à déterminer
        if ($this->pv < 1) {
            $this->state = 5;
        }
        $this->updated = false;
    }

    /**
     * @param int $idRef
     *
     * @return static
     */
    public function setIdRef(int $idRef) {
        $this->idRef = $idRef;
        return $this;
    }

    /**
     * @param bool $updated
     *
     * @return static
     */
    public function setUpdated(bool $updated) {
        $this->updated = $updated;
        return $this;
    }

    //==================================================
    // GETTERS
    //==================================================

    /**
    * @return bool
    * Est-ce que la carte est un bouclier ?
    */
    public function isShield(): bool {
        return ($this->type === 2);
    }


    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getType(): int {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTypeStr(): string {
        return ['créature', 'bouclier', 'sort'][$this->type];
    }

    /**
     * @return int
     */
    public function getPV(): int {
        return $this->pv;
    }

    /**
     * @return int
     */
    public function getPF(): int {
        return $this->pf;
    }

    /**
     * @return int
     */
     public function getCost(): int {
        return $this->cost;
    }

    /**
     * @return string|null
     */
    public function play() {
        return $this->spell;
    }

    /**
     * @return int
     */
    public function getState(): int {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getDeckorder(): int {
        return $this->deckOrder;
    }

    /**
     * @return int
     */
    public function getIdRef(): int {
        return $this->idRef;
    }

    /**
     * @return bool
     */
    public function getUpdated(): bool {
        return $this->updated;
    }

    /**
     * @return string
     */
    public function getImage(): string {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return static
     */
    public function setImage(string $image) {
        $this->image = $image;
        return $this;
    }

}
