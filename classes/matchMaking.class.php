<?php

require_once(CLASSESPATH.'MyPdo.class.php');

/**
 *
 */
abstract class MatchMaking {

    /**
     * @var array
     */
    private static $datasSave = [];


    /**
     * @param int $userId
     *
     * @return bool
     */
    private function loadPlayerData( int $userId = 0 ): bool {
        if ( $userId === 0 ) {
            $userId = $_SESSION['id'];
        }
        $str = '
            SELECT `'.DBPREFIX.'players`.*, `factions`.`name` as faction_name
            FROM `'.DBPREFIX.'players`
            INNER JOIN `factions` ON `'.DBPREFIX.'players`.`id_faction` = `factions`.`id`
            WHERE `id_user` = :idUser AND (`'.DBPREFIX.'players`.`state` = 1 OR `'.DBPREFIX.'players`.`state` = 2)
            ORDER BY `'.DBPREFIX.'players`.`id` DESC
            LIMIT 1';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'idUser', $userId ) ) {
                if ( $sql->execute() ) {
                    $datas = $sql->fetchAll( PDO::FETCH_ASSOC );
                    if (count( $datas ) !== 1) {
                        return false;
                    }
                    self::$datasSave[$userId] = $datas[0];
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param int $userId
     *
     * @return int
     */
    private function initUser( int $userId = 0 ): int {
        if ( $userId === 0 ) {
            $userId = $_SESSION['id'];
        }
        if ( !isset( self::$datasSave[$userId] ) ) {
            self::loadPlayerData( $userId );
        }
        return $userId;
    }

    /**
     * Cherche dans la base de donnée un player d'une partie en cours (2) ou en attente d'un adversaire (1)
     * @param int $userId
     *
     * @return int  : id du joueur, ou 0 si aucun player  trouvé
     */
    public function getPlayerState( int $userId = 0 ): int {
        $userId = self::initUser( $userId );
        return isset( self::$datasSave[$userId] ) ? self::$datasSave[$userId]['state'] : 0;
    }

    /**
     * Va chercher la partie en cours ou en attente du joueur, retourne 0 si aucune
     * @param int $userId
     *
     * @return int
     */
    public function getGameIdFromUserId( int $userId = 0 ): int {
        $userId = self::initUser( $userId );
        return isset( self::$datasSave[$userId] ) ? self::$datasSave[$userId]['id_game'] : 0;
    }

    /**
     * Va chercher la faction du joueur en attente, retourne '' si aucune
     * @param int $userId
     *
     * @return string
     */
    public function getPlayerFaction( int $userId = 0 ): string {
        $userId = self::initUser( $userId );
        return isset( self::$datasSave[$userId] ) ? self::$datasSave[$userId]['faction_name'] : '';
    }

    /**
     * Va chercher l'id d'une partie en attente et en retourne l'id, [] si problème
     * @param int $userId
     *
     * @return array ["opponentId", "advFactionId"]
     */
    public function findOpponent( int $userId = 0 ): array {
        $userId = self::initUser( $userId );
        $str =
            'SELECT `'.DBPREFIX.'players`.*, `factions`.`name` as faction_name
            FROM `'.DBPREFIX.'players`
            INNER JOIN `factions` ON `'.DBPREFIX.'players`.`id_faction` = `factions`.`id`
            WHERE `id_user` != :idUser AND `'.DBPREFIX.'players`.`state` = 1
        ';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'idUser', $userId ) ) {
                if ( $sql->execute() ) {
                    $datas = $sql->fetchAll( PDO::FETCH_ASSOC );
                    if (count($datas) < 1) {
                        return [];
                    }
                    return ['opponentId'=> +$datas[0]['id'], 'advFactionId' => +$datas[0]['id_faction']];
                    // NOTE: par défaut on retourne le premier, mais à voir ...
                }
            }
        }
        return [];
    }
}
