<?php

require_once(CLASSESPATH.'myPdo.class.php');
require_once(CLASSESPATH.'manager.class.php');
require_once(CLASSESPATH.'game.class.php');


/**
 * Classe abstraite pour gérer les accès BDD du Game
 */
abstract class GameManager extends Manager {

    /**
     * @param int $idGame
     *
     * @return array : datas or [] if error
     */
    public function load( int $idGame ): array {
        $str =
            'SELECT  * FROM `'.DBPREFIX.'games`
            WHERE `id` = :id';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'id', $idGame ) ) {
                    if ( $sql->execute() ) {
                    $datas = $sql->fetchAll( PDO::FETCH_ASSOC );
                    if ( count( $datas ) !== 1 ) {
                        return [];
                    }
                    return $datas[0];
                }
            }
        }
        return [];
    }

    /**
     * @return int $idGame, 0 if failure
     */
    public function create(): int {
        $str = 'INSERT INTO `'.DBPREFIX.'games` (`turn`, `id_first_player`) VALUES (0, 0);';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->execute() ) {
                return MyPdo::getInstance()->lastInsertId();
            }
        }
        return 0;
    }

    /**
     * @param Game $game
     *
     * @return bool
     */
    public function updateFirstPlayer( Game $game ): bool {
        $str = 'UPDATE `'.DBPREFIX.'games` SET `id_first_player` = :idFirstPlayer WHERE `' . DBPREFIX . 'games`.`id` = :gameId;';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'idFirstPlayer', $game->getFirstPlayerId() ) & $sql->bindValue( 'gameId', $game->getId() ) ) {
                if ( $sql->execute() ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param Game $game
     *
     * @return bool
     */
    public function update( Game $game ): bool {
        $str = 'UPDATE `'.DBPREFIX.'games` SET `turn` = :turn WHERE `dev_games`.`id` = :gameId;';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'turn', $game->getTurn() ) & $sql->bindValue( 'gameId', $game->getId() ) ) {
                if ( $sql->execute() ) {
                    $game->setUpdated( true );
                    return true;
                }
            }
        }
        return false;
    }

}
