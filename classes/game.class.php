<?php

/**
 * Classe game, qui va contrôler les classes joueurs et cards
 */
class Game {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dateBegin;

    /**
     * @var int
     */
    private $turn;

    /**
     * @var int
     */
    private $firstPlayerId;

    /**
     * @var bool
     */
    private $loaded;

    /**
     * @var bool
     */
    private $updated;
    /**
     * @var Player
     */
    private $myself;
    /**
     * @var Player
     */
    private $opponent;

    /**
     *
     */
    public function __construct() {
        $this->setLoaded( false );
    }

    /**
     * @param array $datas
     *
     * @return bool
     */
    public function load( array $datas ): bool {
        if (
            isset( $datas['id'] ) && is_numeric( $datas['id'] ) && +$datas['id'] > 0 &&
            isset( $datas['dateBegin']) &&
            isset( $datas['turn'] ) && is_numeric( $datas['turn'] ) && +$datas['turn'] >= 0 &&
            isset( $datas['id_first_player'] ) && is_numeric( $datas['id_first_player'] ) && +$datas['id_first_player'] > 0
        ) {
            $this
                ->setId( +$datas['id'] )
                ->setDateBegin( $datas['dateBegin'] )
                ->setTurn( +$datas['turn'] )
                ->setFirstPlayerId( +$datas['id_first_player'] )
                ->setLoaded( true )
                ->setUpdated( true );
            return true;
        }
        return false;
    }


    //-----------------------------------------------------
    // Getters
    //-----------------------------------------------------

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDateBegin(): string {
        return $this->dateBegin;
    }

    /**
     * @return int
     */
    public function getTurn(): int {
        return $this->turn;
    }

    /**
     * @return int
     */
    public function getFirstPlayerId(): int {
        return $this->firstPlayerId;
    }

    /**
     * @return bool
     */
    public function getLoaded(): bool {
        return $this->loaded;
    }

    /**
     * @return bool
     */
    public function getUpdated(): bool {
        return $this->updated;
    }

    /**
     * @return Player
     */
    public function getMyself(): Player {
        return $this->myself;
    }

    /**
     * @return Player
     */
    public function getOpponent(): Player {
        return $this->opponent;
    }

    //-----------------------------------------------------
    // Setters
    //-----------------------------------------------------

    /**
     * @param int $id
     *
     * @return static
     */
    public function setId(int $id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $dateBegin
     *
     * @return static
     */
    public function setDateBegin(string $dateBegin) {
        $this->dateBegin = $dateBegin;
        return $this;
    }

    /**
     * @param int $turn
     *
     * @return static
     */
    public function setTurn(int $turn) {
        $this->turn = $turn;
        return $this;
    }

    /**
     * @param int $firstPlayerId
     *
     * @return static
     */
    public function setFirstPlayerId(int $firstPlayerId) {

        $this->firstPlayerId = $firstPlayerId;
        return $this;
    }

    /**
     * @param bool $loaded
     *
     * @return static
     */
    public function setLoaded(bool $loaded) {
        $this->loaded = $loaded;
        return $this;
    }

    /**
     * @param bool $updated
     *
     * @return static
     */
    public function setUpdated(bool $updated) {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @param Player $myself
     *
     * @return static
     */
    public function setMyself(Player $myself) {
        $this->myself = $myself;
        return $this;
    }

    /**
     * @param Player $opponent
     *
     * @return static
     */
    public function setOpponent(Player $opponent) {
        $this->opponent = $opponent;
        return $this;
    }







    //-----------------------------------------------------
    // Creation de nouvelle partie
    //-----------------------------------------------------


    //-----------------------------------------------------
    // Chargement de partie deja existante
    //-----------------------------------------------------

    // public function loadMyself(int $id) {
    //     if ($id < 1) {
    //         return false;
    //     }
    //     return $this->getMyself()->load($id);
    // }
    //
    // public function loadOpponent(int $id) {
    //     if ($id < 1) {
    //         return false;
    //     }
    //     return $this->getOpponent()->load($id);
    // }



    // /**
    //  * @param Player $myself
    //  * @param Player $opponent
    //  */
    // public function loadPlayers(Player $myself, Player $opponent) {
    //     $this->getMyself() = $myself;
    //     $this->getOpponent() = $opponent;
    // }

    /**
     * @return bool
     * calcule la fin de tour : incrémente le tour, passe les cartes en 'rest' dans le 'board'
     * retour True si la partie est finie !!!
     */
    function endTurn(): bool {
        $endGame = $this->checkEndGame();
        $this->getActivePlayer()->endTurn();                            //on lance la fonction de fin de tour du joueur actif
        $this->turn += 1;                                               //on incrémente le tour
        $this->getActivePlayer()->setPm(min(10, intdiv( $this->getTurn() , 2) ) );      //on met à jour le mana du nouveau premier joueur
        $this->getActivePlayer()->draw(1);                              //on le fait piocher
        $this->updated = false;                                         //et enfin on met à jour la base de donnée pour la partie (y'a que le tour qui est concerné)
        return $endGame;
    }


//à dégager
    /**
     * @return bool
     * TODO : vérifier que tout se soit bien passé, et retourner false si problème
     */
    public function updateDB(): bool {
        if (!$this->updated = false) {
            $sql=$this->pdo->prepare('UPDATE `'.DBPREFIX.'games` SET `turn` = :turn WHERE `'.DBPREFIX.'games`.`id` = :idGame');
            $sql->execute(['turn'=>$this->turn, 'idGame'=>$this->id]);
        }
        $this->getMyself()->updateDB();
        $this->getOpponent()->updateDB();
        return ($this->updated = true);
    }


    /**
     * @param null|string $spell
     */
    function playSpell($spell) {
        switch ($spell) {
            case NULL:
                //echo 'pas un sort !!!!';
                break;
            case 'soin':
                $this->getMyself()->heal(3);
                break;
            case 'bombe':
                $this->getOpponent()->hurt(3);
                $this->checkEndGame();
                break;
            case 'pioche':
                $this->getMyself()->draw(3);
                break;
            default:
                # code...
                break;
        }
        $this->updated = false;
    }

    /**
     * @param int $idCardAttacking
     */
    public function fightHero(int $idCardAttacking) {
        $cA = $this->getMyself()->getCards()[$idCardAttacking];
        $this->getOpponent()->hurt($cA->getPF());
        $this->getMyself()->moveCards($cA, 'Rest');
        $this->updated = false;
    }

    /**
     * @param int $idCardAttacking
     * @param int $idCardTargeted
     */
    public function fightCreatures(int $idCardAttacking, int $idCardTargeted) {
        $cA = $this->getMyself()->getCards()[$idCardAttacking];
        $cT = $this->getOpponent()->getCards()[$idCardTargeted];
        $this->getMyself()->moveCards($cA, 'Rest');
        $cA->hurt($cT->getPF());
        $cT->hurt($cA->getPF());
        $this->updated = false;
    }

    /**
     * @return bool
     */
    public function checkEndGame(): bool {
        //si la partie n'est pas déjà finie, on demande à chaque joueur
        if ( $this->getMyself()->checkEndGame() || $this->getOpponent()->checkEndGame() ) {
            return true;
            // $this->getMyself()->setState(3);
            // $this->getOpponent()->setState(3);
        }
        return false;
    }

    /**
     * @return array
     * retourne la liste des joueurs
     */
    public function getPlayers(): array {
        return [$this->getMyself(), $this->getOpponent()];
    }

    /**
     * @return Player|bool
     * retourne le joueur actif
     */
    public function getActivePlayer() {
         return [$this->getMyself(), $this->getOpponent()][($this->turn + ($this->getMyselfId() == $this->firstPlayerId ? 1 : 2))%2];
    }

    /**
     * @return Player|bool
     * retourne le joueur actif
     */
    public function getNotActivePlayer() {
         if ($this->turn>0) {
            return [$this->getMyself(),$this->getOpponent()][($this->turn+$this->firstPlayer)%2];
        } else {
            return false;
        }
    }

    /**
     * @return int
     * retourne l'id du joueur actif
     */
    public function getActivePlayerId(): int {
        return $this->getActivePlayer()->getId();
    }

    /**
     * @return int
     * retourne l'id du joueur actif
     */
    public function getNotActivePlayerId(): int {
        return $this->getNotActivePlayer()->getId();
    }

    /**
     * @return bool
     */
    public function amIActivePlayer(): bool {
        return $this->getActivePlayerId() == $this->getMyselfId();
    }

    /**
     * @return int
     */
    public function getMyselfId(): int {
        return $this->getMyself()->getId();
    }



}
