<?php

require_once(CLASSESPATH.'myPdo.class.php');
require_once(CLASSESPATH.'manager.class.php');
require_once(CLASSESPATH.'player.class.php');
require_once(CLASSESPATH.'cardManager.class.php');

/**
 * Classe abstraite pour instancier un player
 */
abstract class PlayerManager extends Manager {


    /**
     * @param int $idUser
     * @param int $idGame
     * @param bool $opponent : true = on cherche à charger l'adversaire, pas nous même
     *
     * @return array $data
     */
    public function load( int $idUser, int $idGame, bool $opponent = false ) {
        // TODO: ajouter des conditions pour vérifier que tout se passe bien
        $str = '
            SELECT `'.DBPREFIX.'players`.*, `users`.`pseudo` FROM `'.DBPREFIX.'players`
            INNER JOIN `users` ON `'.DBPREFIX.'players`.`id_user` = `users`.`id`
            INNER JOIN `factions` ON `'.DBPREFIX.'players`.`id_faction` = `factions`.`id`
            WHERE `'.DBPREFIX.'players`.`id_user` '.($opponent ? '!' : '').'= :idUser AND `'.DBPREFIX.'players`.`id_game` = :idGame
        ';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue('idUser', $idUser) && $sql->bindValue('idGame', $idGame) ) {
                if ( $sql->execute() ) {
                    $datas = $sql->fetchAll(PDO::FETCH_ASSOC);
                    if (count($datas) !== 1) {      //ici ce n'est pas sensé se produire, mais on est jamais trop prudent ...
                        return [];
                    }
                    return $datas[0];
                }
            }
        }
        return [];
    }

    /**
     * @param Player $player
     *
     * @return bool
     */
    public function updateGameId( Player $player ): bool {
        // if ($player->getUpdated()) {
        //     # code...
        // }
        $str = 'UPDATE `'.DBPREFIX.'players` SET `id_game` = :idGame, `state` = :state WHERE `'.DBPREFIX.'players`.`id` = :idPlayer;';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'idGame', $player->getGameId() ) && $sql->bindValue( 'state', $player->getState() )&& $sql->bindValue( 'idPlayer', $player->getId() ) ) {
                if ( $sql->execute() ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Met à jour la player dans la base de données, seulement si nécessaire (updated == false)
     * @param Player $player
     *
     * @return bool
     */
    public function update( Player $player ): bool {
        if ( !$player->getUpdated() ) {
            $str = 'UPDATE `'.DBPREFIX.'players` SET `pv` = :pv, `pm` = :pm WHERE `'.DBPREFIX.'players`.`id` = :idPlayer;';
            if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
                if ( $sql->bindValue( 'pv', $player->getPv() ) && $sql->bindValue( 'pm', $player->getPm() ) && $sql->bindValue( 'idPlayer', $player->getId() ) ) {
                    if ( $sql->execute() ) {
                        $player->setUpdated( true );
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public function updateState( Player $player ): bool {
        $str = 'UPDATE `'.DBPREFIX.'players` SET `state` = :state WHERE `'.DBPREFIX.'players`.`id` = :idPlayer;';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if ( $sql->bindValue( 'state', $player->getState() ) && $sql->bindValue( 'idPlayer', $player->getId() ) ) {
                if ( $sql->execute() ) {
                    $player->setUpdated( true );
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param int $factionId
     * @param int $idGame
     * @param int $idUser
     *
     * @return int
     */
    public function create( int $factionId, $idGame = 0, $idUser = 0 ): int {
        if ( $idUser === 0 ) {
            $idUser = $_SESSION['id'];
        }
        $str =
            'INSERT INTO `'.DBPREFIX.'players` (`id_user`, `id_faction`,`id_game`, `pv`, `pm`, `state`) VALUES
            (:idUser, :idFaction, :idGame, '.STARTING_LIFE.', 0, :state);';
        if ( ( $sql = MyPdo::getInstance()->prepare( $str ) ) !== false ) {
            if (
                $sql->bindValue( 'idUser', $idUser ) &&
                $sql->bindValue( 'idFaction', $factionId ) &&
                $sql->bindValue( 'idGame', $idGame ) &&
                $sql->bindValue( 'state', ( $idGame === 0 ? 1 : 2 ) )
            ) {
                if ( $sql->execute() ) {
                    return MyPdo::getInstance()->lastInsertId();
                }

            }
        }
        return 0;
    }

    /**
     * Charge les cartes du joueur, mélange, pioche la main de départ
     * @param Player $player
     */
    public function cardsInit( Player $player ) {
        $cardsRef = CardManager::getCardsRef( $player->getFactionId() );
        CardManager::registerCards( $cardsRef, $player->getId() );
        $cards = CardManager::loadCards( $player->getId() );
        shuffle( $cards );
        $player->moveCards( $cards, 'Deck' );
        $player->draw( 3 );
        // var_dump( $player->getDeck() );
        // var_dump( $cards );
        CardManager::updateCards( $cards, $player->getId() );
    }

    /**
     * @param Player $player
     */
    public function loadCards( Player $player ) {
        $str='
           SELECT `'.DBPREFIX.'cards`.`id` as `id`, `'.DBPREFIX.'cards`.`state` as `state`,  `cardsref`.`name` as `name`, `cardsref`.`description` as `description`, `cardsref`.`type` as `type`, `cardsref`.`cost` as `cost`, `'.DBPREFIX.'cards`.`pv` as `pv`, `cardsref`.`pf` as `pf`, `cardsref`.`spell` as `spell`, `cardsref`.`id_faction` as `faction`, `cardsref`.`image` as `image`, `'.DBPREFIX.'cards`.`deck_order` as `deckOrder`
           FROM `'.DBPREFIX.'cards`
           INNER JOIN `cardsref` ON `'.DBPREFIX.'cards`.`id_cardref` = `cardsref`.`id`
           WHERE `'.DBPREFIX.'cards`.`id_player` = :idPlayer
           ORDER BY `deck_order`,`id` DESC
           ';
           if ( $sql = MyPdo::getInstance()->prepare( $str ) ) {
               if ( $sql->bindValue( 'idPlayer', $player->getId() ) ) {
                   if ( $sql->execute() ) {
                       while ( $dataCard = $sql->fetch( PDO::FETCH_ASSOC ) ) {
                           $card = new Card(
                               $dataCard['name'],
                               $dataCard['description'],
                               +$dataCard['type'],
                               +$dataCard['cost'],
                               +$dataCard['pv'],
                               +$dataCard['pf'],
                               $dataCard['spell'],
                               +$dataCard['faction'],
                               $dataCard['image'],
                               +$dataCard['state'],
                               +$dataCard['deckOrder']
                           );
                           $card->setId( +$dataCard['id'] );
                           switch ( $card->getState() ) {
                               case Deck::STATE :
                                    $player->moveCards( $card, 'Deck' );
                                    break;
                                case Hand::STATE :
                                    $player->moveCards( $card, 'Hand' );
                                    break;
                                case Rest::STATE :
                                    $player->moveCards( $card, 'Rest' );
                                    break;
                                case Board::STATE :
                                    $player->moveCards( $card, 'Board' );
                                    break;
                                case Discard::STATE :
                                    $player->moveCards( $card, 'Discard' );
                                    break;
                               default:
                                   throw new \Exception( 'Card State unknow ! ');
                                   break;
                           }
                       }
                       //prepare, conditionnel à faire
                       $sql->closeCursor();

                       return true ;
                   } else {
                       //echo 'error execute set';
                       return false;
                   }
               } else {
                   //echo 'error prepare set';
                   return false;
               }
           } else {
               //echo 'error prepare set';
               return false;
           }


    }

}
