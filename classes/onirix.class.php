<?php

require_once(CLASSESPATH.'myPdo.class.php');
require_once(CLASSESPATH.'playerManager.class.php');
require_once(CLASSESPATH.'game.class.php');
require_once(CLASSESPATH.'matchMaking.class.php');

/**
 * Classe de plus haut niveau, instancie et contrôle tout le reste
 */
class Onirix {

    /**
     * @var Player
     */
    private $myself;

    /**
     * @var Player
     */
    private $opponent;

    /**
     * @var Game
     */
    private $game;

    /**
     * @var bool
     */
    private $loaded;

    /**
     * @var bool
     */
    private $updated;


    /**
     *
     */
    function __construct() {
        $this->setLoaded(false)->setGame( new Game() )->setMyself( new Player() )->setOpponent( new Player() );
    }


    //-----------------------------------------------------
    // Getters
    //-----------------------------------------------------

    /**
     * @return Player
     */
    public function getMyself(): Player {
        return $this->myself;
    }

    /**
     * @return Player
     */
    public function getOpponent(): Player {
        return $this->opponent;
    }

    /**
     * @return bool
     */
    public function getLoaded(): bool {
        return $this->loaded;
    }

    /**
     * @return Game
     */
    public function getGame(): Game {
        return $this->game;
    }

    /**
     * @return bool
     */
    public function getUpdated(): bool {
        return $this->updated;
    }

    //-----------------------------------------------------
    // Setters
    //-----------------------------------------------------

    /**
     * @param Player $myself
     *
     * @return static
     */
    public function setMyself(Player $myself) {
        $this->myself = $myself;
        return $this;
    }

    /**
     * @param Player $opponent
     *
     * @return static
     */
    public function setOpponent(Player $opponent) {
        $this->opponent = $opponent;
        return $this;
    }

    /**
     * @param Game $game
     *
     * @return static
     */
    public function setGame(Game $game) {
        $this->game = $game;
        return $this;
    }

    /**
     * @param bool $loaded
     *
     * @return static
     */
    public function setLoaded(bool $loaded) {
        $this->loaded = $loaded;
        return $this;
    }

    /**
     * @param bool $updated
     *
     * @return static
     */
    public function setUpdated(bool $updated) {
        $this->updated = $updated;
        return $this;
    }


    //-----------------------------------------------------
    // Creation de nouvelle partie
    //-----------------------------------------------------

    /**
     * Crée un nouveau player dans la base de donnée
     * @param int $factionId
     *
     * @return int l'id du player créé
     */
    public function createPlayer( int $factionId ): int {
        return PlayerManager::create( $factionId );
    }

    /**
     * @param int $opponentId
     * @param int $advFactionId
     *
     * @return bool
     */
    public function createGame( int $opponentId, int $advFactionId ): bool {
        //initialisation des objets
        $this
            ->setGame( new Game() )
            ->setMyself( new Player() )
            ->setOpponent( new Player() );
        if ( ( $idGame = GameManager::create() ) !== 0 ) {

            //Myself stuff
            $factionId = ( $advFactionId === 1 ? 2 : 1 );       // TODO: faire quelquechose de plus générique peut être, en cas d'ajout de faction ...
            $idMyself = PlayerManager::create( $factionId, $idGame, $_SESSION['id'] );
            $this->getMyself()->setId( $idMyself )
                                ->setPv( STARTING_LIFE )
                                ->setPm( 0 )
                                ->setFactionId( $factionId );
            PlayerManager::cardsInit( $this->getMyself() );
            PlayerManager::update( $this->getMyself() );

            //Game stuff
            $this->getGame()->setId( $idGame );
            $fPlayer = [$idMyself, $opponentId][array_rand( [$idMyself, $opponentId] )];
            $this->getGame()->setFirstPlayerId( $fPlayer );
            GameManager::updateFirstPlayer( $this->getGame() );
            $this->getGame()->setTurn( 1 );
            GameManager::update( $this->getGame() );


            //Opponent stuff
            $this->getOpponent()->setGameId( $idGame )
                                ->setId( $opponentId )
                                ->setPv( STARTING_LIFE )
                                ->setPm( 0 )
                                ->setFactionId( $advFactionId )
                                ->setState( 2 );
            PlayerManager::updateGameId( $this->getOpponent() );
            PlayerManager::cardsInit( $this->getOpponent() );
            PlayerManager::update( $this->getOpponent() );

            if ( $fPlayer === $idMyself) {
                $this->getMyself()->setPm( 1 )->setUpdated( false );
                PlayerManager::update( $this->getMyself() );
            } else {
                $this->getOpponent()->setPm( 1 )->setUpdated( false );
                PlayerManager::update( $this->getOpponent() );
            }

            //Session stuff
            $_SESSION['playerId'] = $idMyself;
            $_SESSION['gameId'] = $idGame;

            return true;
        }
        return false;
    }


    //-----------------------------------------------------
    // Chargement de partie en cours
    //-----------------------------------------------------


    /**
     * @param int|null $idGame
     */
    function load(int $idGame = null) {
        if ( $idGame === null ) {
            //besoin de retrouver l'id game
            $idGame = MatchMaking::getGameIdFromUserId( $_SESSION['id'] );
        }
        if (
            $this->getGame()->load( GameManager::load( $idGame ) ) &&
            $this->getMyself()->load( PlayerManager::load( $_SESSION['id'], $idGame ) ) &&
            $this->getOpponent()->load( PlayerManager::load( $_SESSION['id'], $idGame, true ) )
        ) {
            $this->getGame()->setMyself( $this->getMyself() );
            $this->getGame()->setOpponent( $this->getOpponent() );
            $this->setLoaded( true );
        }

    }


    //-----------------------------------------------------
    // OLD
    //-----------------------------------------------------

    public function checkMyself(): bool {
        if (isset($_SESSION['idMyself'])) {
            if (!isset($this->myself)) {
                $this->setMyself(PlayerManager::loadPlayer($_SESSION['idMyself']));
            }
            return true;
        }

    }

    /**
     * @param int $idUser
     * @param int $gameId
     * @return array|false retourne false si un problème est survenu, sinon les infos pour le joueur
     */
    private function playerLoad($idUser, $gameId = NULL) {
        /*
        Va chercher un joueur en bdd et retourne l'objet
        Deux manières différentes de foncitonner :
            on fournit juste l'id d'un player, dans ce cas on ne connais pas l'id de la partie, et il nous faut aller récupérer le dernier player avec cet id
            on fournit l'id d'un player (myself) et d'une partie, dans ce cas on doit aller chercher l'adversaire (opponent) en prenant le player qui correspond à cette partie et qui n'est pas myself
        */

        if ($gameId === NULL) {
            $str = '
                SELECT `'.DBPREFIX.'players`.*, `users`.`pseudo`, `users`.`league`, `users`.`avatar` FROM `'.DBPREFIX.'players`
                INNER JOIN `users` ON `'.DBPREFIX.'players`.`id_user` = `users`.`id`
                WHERE `id_user` = :idUser AND (`'.DBPREFIX.'players`.`state` = 1 OR `'.DBPREFIX.'players`.`state` = 2)
                ORDER BY `'.DBPREFIX.'players`.`id` DESC
                LIMIT 1';
                $sql = MyPdo::getInstance()->prepare($str);           //ici on peut rajouter des if pour traiter les erreurs ...
                $sql->bindValue('idUser', $idUser);
                $sql->execute();
        } else {
            $str = '
                SELECT `'.DBPREFIX.'players`.*, `users`.`pseudo`, `users`.`league`, `users`.`avatar` FROM `'.DBPREFIX.'players`
                INNER JOIN `users` ON `'.DBPREFIX.'players`.`id_user` = `users`.`id`
                WHERE `id_user` != :idUser AND `id_game` = :gameId';
                $sql = MyPdo::getInstance()->prepare($str);
                $sql->bindValue('idUser', $idUser);         //idem
                $sql->bindValue('gameId', $gameId);
                $sql->execute();
        }

        $data = $sql->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) != 1) {
            //Là en cas de jeu à 3+ joueurs possibles, y'aura du changement à faire
            return false;
        }
        // $player = new Player($this->pdo);
        // $player->load();
        return $data[0];
    }

    /**
     * @param int $gameId
     * Va chercher la partie en bdd et retourne l'objet
     */
    private function gameLoad(int $gameId) {
        $sql = MyPdo::getInstance()->prepare('
            SELECT  * FROM `'.DBPREFIX.'games`
            WHERE `id` = :gameId
        ');
        $sql->execute(['gameId'=> $gameId]);
        $data = $sql->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) != 1) {
            return false;
        }
        $game = new Game();
        $game->load($data[0]);
        return $game;
    }

    /**
     * @return bool
     */
    public function checkEndGame(): bool {
        return $this->game->checkEndGame();
    }

    /**
     * Update du game (qui lui même va lancer l'update des players, et ainsi de suite)
     */
    public function updateDB() {
        GameManager::update( $this->getGame() );
        PlayerManager::update( $this->getMyself() );
        CardManager::updateCards( $this->getMyself()->getCards(), $this->getMyself()->getId() );
        PlayerManager::update( $this->getOpponent() );
        CardManager::updateCards( $this->getOpponent()->getCards(), $this->getOpponent()->getId() );
        $this->setUpdated(true);
    }


}
