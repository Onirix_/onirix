<?php

session_start();

define('DEV', true);        //NOTE: à dégager quand on passe en prod !

if ((isset($_SESSION['id']) && $_SESSION['id']<4) || defined('DEV')) {    //détermine si l'utilisateur est un développeur, s'il l'est on utilisera la base de donnée de développement
    define( 'DBPREFIX', 'dev_');                      // NOTE: il n'y a pas encore de base de donnée non-dev, mais l'idée c'est de pouvoir (plus tard), passer facilement de l'une à l'autre
    ini_set("display_errors", 1);                     //au passage on en profite pour activer les erreurs
} else {
    define( 'DBPREFIX', '');
    ini_set("display_errors", 0);
}
// NOTE: il faudrait vérifier cette histoire de "display_errors", il y a plein d'options possibles



/*
ce qui suit est directement repris de l'exo de Damien, à adapter à notre sauce au fur et à mesure
*/

if( strtoupper( substr( PHP_OS, 0, 3 ) ) == 'WIN' ) : // If the version of the operating system (provided by the pre-defined constants PHP_OS) corresponds to a Windows kernel,
    if( !defined( 'PHP_EOL') ) : define( 'PHP_EOL', "\r\n" ); endif;
    if( !defined( 'PATH_SEPARATOR') ) : define( 'PATH_SEPARATOR', ';' ); endif;
    if( !defined( 'DIRECTORY_SEPARATOR') ) : define( 'DIRECTORY_SEPARATOR', "\\" ); endif;
else :
    if( !defined( 'PHP_EOL') ) : define( 'PHP_EOL', "\n" ); endif;
    if( !defined( 'PATH_SEPARATOR') ) : define( 'PATH_SEPARATOR', ':' ); endif;
    if( !defined( 'DIRECTORY_SEPARATOR') ) : define( 'DIRECTORY_SEPARATOR', "/" ); endif;
endif;

if( !defined( 'DS' ) )
    define( 'DS', DIRECTORY_SEPARATOR ); // Defines the folder separator connected to the system
/** **/

if( !defined( 'ABSPATH' ) )
    define( 'ABSPATH', __DIR__ . DS ); // Defines the root folder
if( !defined( 'CONFIGPATH' ) )
    define( 'CONFIGPATH', ABSPATH . 'app' . DS . 'config' . DS ); // Defines the config folder

//require_once( CONFIGPATH . 'framewind.conf' );
//require_once( CONFIGPATH . 'app.conf' );
//require_once( VENDORPATH . 'common.php' );


if( !defined( 'DOMAIN' ) )
    define( 'DOMAIN', ( isset( $_SERVER['REQUEST_SCHEME'] ) ? $_SERVER['REQUEST_SCHEME'] : 'http' ) . '://' . $_SERVER['SERVER_NAME'] . ( !empty( $_SERVER['SERVER_PORT'] ) ? ':' . $_SERVER['SERVER_PORT'] : '' ) . dirname( $_SERVER['PHP_SELF'] ) . '/' );
if( !defined( 'THEME_URL' ) )
    define( 'THEME_URL', DOMAIN ); // Defines the path to the folder containing the files to include
if( !defined( 'ASSETS_URL' ) )
    define( 'ASSETS_URL', THEME_URL . 'web/assets/' ); // Defines the path to the folder containing the files to include
if( !defined( 'VENDORPATH' ) )
    define( 'VENDORPATH', ABSPATH . 'vendor' . DS ); // Defines the path to the folder containing the third-party dependencies
if( !defined( 'APPPATH' ) )
    define( 'APPPATH', ABSPATH . 'app' . DS ); // Defines the path to the folder containing the application configuration and translations
if( !defined( 'LOGSPATH' ) )
    define( 'LOGSPATH', ABSPATH . 'var' . DS . 'logs' . DS ); // Defines the path to the folder containing the logs
if( !defined( 'WEBPATH' ) )
    define( 'WEBPATH', ABSPATH ); // Defines the path to the folder containing the web root directory
if( !defined( 'ASSETSPATH' ) )
    define( 'ASSETSPATH', WEBPATH . 'assets' . DS ); // Defines the path to the folder containing the assets files

/*
Fin Damien
*/

if (!defined('SITE_NAME')) {
    define('SITE_NAME', 'Onirix');
}
if (!defined('SITE_TITLE')) {
    define('SITE_TITLE', 'Onirix');
}
if (!defined('DESCRIPTION')) {
    define('DESCRIPTION', 'Onirix, the game');
}
if (!defined('FOLDER')) {
    define('FOLDER','onirix');
}

if (!defined('MAINFOLDER')) {
    define('MAINFOLDER', DS.FOLDER.DS);
}

if (!defined('RESULTS_PER_PAGE'))
    define('RESULTS_PER_PAGE', 20);

if( !defined( 'CLASSESPATH' ) ) {           //le chemin vers les classes du jeu
    define( 'CLASSESPATH', WEBPATH . 'classes' . DS ); // Defines the path to the folder containing the assets files
}
if( !defined( 'CONTROLLERSPATH' ) ) {
    define( 'CONTROLLERSPATH', WEBPATH . 'controllers' . DS );
}

if (!defined('GAMEADRESS')) {
    define('GAMEADRESS', DOMAIN.'game.php');
}


//BDD
require_once(ABSPATH.DS.'server_config'.DS.'bdd_access.php');

//Game Const
require_once( ABSPATH.DS.'gameConfig.php');
