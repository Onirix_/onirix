<?php

require_once('./ini.php');

require_once('./classes/onirix.class.php');

$pdo = new MYPDO(PDODSN, PDOUSERNAME, PDOPASSWORD);

$o = new Onirix($pdo);

$o->checkMyself();
$o->gameRunning();
$o->load();
