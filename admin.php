<?php 
session_start();
require_once('common.php');



if(isset($_SESSION['role']) && ( $_SESSION['role'] === 1 || $_SESSION['role'] === 2)) {
    $carte = makeSelect('SELECT `types`.`name` AS "type_name", `factions`.`name` AS `faction_name`, `cardsref`.* FROM `cardsref`
                         INNER JOIN `types` ON `cardsref`.`type`= `types`.`id`
                         INNER JOIN `factions` ON `cardsref`.`id_faction`= `factions`.`id`;');
                         
   
?>

  <h2> Interface administrateur</h2> 

  
  <form action="" method="GET" >
    <table cellpadding="10px" border="1px" style="border-collapse: collapse">
      <thead>
            <th>Id</th>
            <th>Nom</th>
            <th>Description</th>
            <th>Type</th>
            <th>Cout </th>
            <th>Points de vie </th>
            <th>Points de force</th>
            <th>Sort</th>
            <th>Faction</th>
			<th>Fiche</th>
      </thead>
      <tbody>
    <?php foreach ($carte as $value) { ?>
    
      <tr>
          <td><?php echo $value['id'] ?></td>
          <td><?php echo $value['name'] ?></td>
          <td><?php echo $value['description']?></td>
          <td><?php echo $value['type_name']?></td>
          <td><?php echo $value['cost']?></td>
          <td><?php echo $value['pv']?></td>
          <td><?php echo $value['pf']?></td>
          <td><?php echo $value['spell']?></td>
          <td><?php echo $value['faction_name']?></td>
          <td> <a href="ficheperso.php?id=<?php echo $value['id']?>">Voir la fiche</a></td>
      </tr>

    <?php } ?>
    	</tbody>
    
    </table>

  
  

  </form>

<?php 
}

else {

  header('Location:./game.php'); 
}


