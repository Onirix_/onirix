<?php


$server = true;

// TODO: faire une fonction pour les game controllers qui :
    // instancie server = true (ou un truc mieux plus propre)
    // vérifie les droits et redirige : si pas connecté par exemple

require_once('../ini.php');
require_once(CONTROLLERSPATH.'gameLoad.php');
// dégager ça, faire une autre page qui charge pour le serveur

if ($o->checkEndGame()) {
    echo 'game over !';
    header('Location:../game.php?gameOver');
} else {
    if (!isset($_GET['card']) || !is_numeric($_GET['card']) || +$_GET['card']<0) {
        echo 'Wrong Get card type !';
        header('Location:../');
        exit();
    } else {
        $cardPlayed = +$_GET['card'];
        if (!$o->getGame()->amIActivePlayer()) {
            echo 'Not Active Player';
            header('Location:../');
            exit();
        } else {
            if(!isset($o->getMyself()->getBoard()[$cardPlayed])) {            //si la carte n'est pas dans la main du joueur
                echo 'Card not in board';
                header('Location:../');
                exit();
            } else {
                echo 'ok';
                header('Location:../game.php?attacking='.$cardPlayed);
            }
        }
    }
}
