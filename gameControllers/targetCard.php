<?php

$server = true;


require_once('../ini.php');
require_once(CONTROLLERSPATH.'gameLoad.php');

if ($o->checkEndGame()) {
    // echo 'game over !';
    header('Location:../game.php?gameOver');
} else {
    if (!isset($_GET['card']) || !is_numeric($_GET['card']) || +$_GET['card']<0 || !isset($_GET['target']) || !is_numeric($_GET['target']) || +$_GET['target']<-1) {            //target 0 = héros
        // echo 'Wrong Get card type !';
        header('Location:../game.php?wrondGetCardType');
        exit();
    } else {
        $cardAttacking = +$_GET['card'];
        $cardTargeted = +$_GET['target'];
        if (!$o->getGame()->amIActivePlayer()) {
            // echo 'Not Active Player';
            header('Location:../game.php?notActivePlayer');
            exit();
        } else {
            if (!isset($o->getMyself()->getBoard()[$cardAttacking])){
                // echo 'Card not in board';
                header('Location:../game.php?attackingCardNotInBoard');
                exit();
            } else {

                // TODO : condition plus propre, là c'est un peu sale ... définir en amont si la cible est dans board ou rest OU concat des sets ...
                if ($o->getOpponent()->hasShield() && ($cardTargeted == 0 || (isset($o->getOpponent()->getBoard()[$cardTargeted]) && !$o->getOpponent()->getBoard()[$cardTargeted]->isShield()) || (isset($o->getOpponent()->getRest()[$cardTargeted]) && !$o->getOpponent()->getRest()[$cardTargeted]->isShield()))) {
                    // echo 'Shield in game !';
                    header('Location:../game.php?shieldInGame');
                } else {

                    if ($cardTargeted == 0) { //si on attaque le héros
                        $o->getGame()->fightHero($cardAttacking);
                        $o->updateDB();
                        header('Location:../game.php?');
                    } else {
                        if (!isset($o->getOpponent()->getBoard()[$cardTargeted]) && !isset($o->getOpponent()->getRest()[$cardTargeted])) {
                            // echo 'Target not in game';
                            header('Location:../game.php?targetNotInGame');
                        } else {
                            $o->getGame()->fightCreatures($cardAttacking, $cardTargeted);
                            $o->updateDB();
                            header('Location:../');
                        }
                    }
                }
            }
        }
    }
}
