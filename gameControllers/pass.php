<?php

$server = true;


require_once('../ini.php');
require_once(CONTROLLERSPATH.'gameLoad.php');


if ($o->checkEndGame()) {
    echo 'game over !';
    header('Location:../game.php?gameOver');
} else {
    if (!$o->getGame()->amIActivePlayer()) {
        echo 'Not Active Player';
        header('Location:../game.php?notActivePlayer');
    } else {
        $o->getGame()->endTurn();
        $o->checkEndGame();
        $o->updateDB();
        echo 'true';
        header('Location:../');
    }
}
