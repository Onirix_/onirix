<?php

$server = true;

require_once('../ini.php');
require_once(CONTROLLERSPATH.'gameLoad.php');

if ($o->checkEndGame()) {
    // echo 'game over !';
    header('Location:../game.php?gameOver');
} else {
    if (!isset($_GET['card']) || !is_numeric($_GET['card']) || +$_GET['card']<0) {
        // echo 'Wrong Get card type !';
        header('Location:../game.php?wrongGetCardType');
        exit();
    } else {
        $cardPlayed = +$_GET['card'];
        if (!$o->getGame()->amIActivePlayer()) {
            // echo 'Not Active Player';
            header('Location:../game.php?notActivePlayer');
            exit();
        } else {

            $hand = $o->getMyself()->getHand();
            if(!isset($hand[$cardPlayed])) {            //si la carte n'est pas dans la main du joueur
                // echo 'Card not in hand';
                header('Location:../game.php?Card not in hand');
                exit();
            } else {
                $manaPlayer = $o->getMyself()->getPM();
                $cost = $hand[$cardPlayed]->getCost();
                if ($manaPlayer < $cost) {
                    // echo 'Not enough mana !';
                    header('Location:../game.php?notEnoughMana');
                    exit();
                } else {
                    $o->getGame()->playSpell( $o->getMyself()->playCard( $cardPlayed ) );
                    $o->updateDB();
                    // echo 'true';
                    header('Location:../');
                    exit();
                }
            }
        }
    }
}
