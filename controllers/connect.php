<?php

require_once('../server_config/bdd_access.php');

/**
 * @param PDO $pdo
 * Récupère le joueur dans la base de donnée
**/
function getUser(PDO $pdo) {
    if (isset($_POST['pseudo']) && isset($_POST['password']) && $_POST['pseudo'] && $_POST['password']) {
        $str='SELECT * FROM `users` WHERE `users`.`pseudo` =:pseudo AND `users`.`password` = :password';
        if ($sql = $pdo->prepare($str)) {
            if ($sql->bindValue('pseudo', $_POST['pseudo']) && $sql->bindValue('password', $_POST['password'])) {
                if ($sql->execute()) {
                    $data=$sql->fetch(PDO::FETCH_ASSOC);       //PDO::FETCH_ASSOC permet de n'avoir que l'array en associative (par défaut les infos sont en doublon ...)
                    if($data) {
                        session_start();
                        $_SESSION['id'] = +$data['id'];
                        $_SESSION['pseudo'] = $data['pseudo'];
                        $_SESSION['role'] = +$data['role']; // + transforme un string en int
                        $sql->closeCursor();
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    //echo 'error execute connect';
                    return false;
                }
            } else {
                //echo 'error bindValue connect';
                return false;
            }
        } else {
            //echo 'error prepare connect';
            return false;
        }
    } else {
        return false;
    }
}

try {
    $pdo = new PDO(PDODSN, PDOUSERNAME, PDOPASSWORD);
    if (getUser($pdo)) {          //si ça marche, on redirige vers le jeu

        switch($_SESSION['role']) { // si la personne se connecte a un role de 3 on redirige vers le jeu
            case 3:
            header('Location:../game.php');
            case 2;
            header('Location:../game.php');
            case 1;
            header('Location:../game.php');
        }

    } else {                        //sinon on affiche un petit message, en attendant de faire mieux
        //header('Location:../index.php?err');
        echo 'erreur de connection<br><a href="..">Back</a>';
    }
}
catch(Exception $e) {
    $pdo->rollback();
    echo 'Echec de la connexion à la base de données';
    exit();
}
