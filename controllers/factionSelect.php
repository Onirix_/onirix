<?php
require_once('../ini.php');




require_once(CLASSESPATH.'onirix.class.php');


if (!isset($_GET['faction']) || !is_numeric($_GET['faction']) || $_GET['faction']<1) {
    echo 'Erreur de sélection de la faction';
} else {
    try {
        $o = new Onirix();
        $_SESSION['playerId'] = $o->createPlayer(+$_GET['faction']);
        header('Location:../game.php');
    } catch(Exception $e) {
        echo 'Echec de la connexion à la base de données';
        exit();
    }
}
