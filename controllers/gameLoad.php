<?php


require_once(CLASSESPATH.'playerManager.class.php');
require_once(CLASSESPATH.'gameManager.class.php');
require_once(CLASSESPATH.'onirix.class.php');


//si on est ici, c'est qu'on a bien une partie en cours,

// var_dump($_SESSION);

try {
    $o = new Onirix();
    $o->load( isset( $_SESSION['gameId'] ) ? $_SESSION['gameId'] : null );
    if ( ! isset( $server ) ) {
        # code...
        include_once( './views/header.php' );

        include_once('./views/board.php');
        include_once( './views/footer.php' );
    }

} catch(Exception $e) {
    echo $e;
    echo 'Echec de la connexion à la base de données';
    exit();
}
