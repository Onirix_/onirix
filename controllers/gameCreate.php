<?php


require_once( CLASSESPATH.'playerManager.class.php' );
require_once( CLASSESPATH.'gameManager.class.php' );
require_once( CLASSESPATH.'onirix.class.php' );


try {
    $o = new Onirix();

    if ( $o->createGame( $opponentId, $advFactionId ) ) {

        header( 'Location:./game.php' );
        // include_once( './views/board.php' );
    } else {
        echo 'error creation de partie';
    }

} catch( Exception $e ) {
    echo 'Echec de la connexion à la base de données';
    exit();
}
