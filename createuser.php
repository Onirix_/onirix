
<?php

require_once('server_config/bdd_access.php');

require_once('common.php');

 if( isset( $_POST['pseudo']) && $_POST['pseudo']!='' &&  isset( $_POST['password']) && $_POST['password']!='' &&  isset( $_POST['mail']) && $_POST['mail']!='') {

    $params = ['pseudo' => $_POST['pseudo'], 'password' =>  $_POST['password'], 'mail' => $_POST['mail'] ]; // les parametres que l'ont soumet dans le formulaire

 }

?>

    <h2> Creez votre compte</h2>

    <form action="" method="POST">
        <label for="pseudo"> Pseudo</label>
        <input type="text" name="pseudo" value= <?php if( isset( $_POST['pseudo']) && $_POST['pseudo']!='') {echo $_POST['pseudo'];}?>></input>
        <br> <br>
        <label for="password"> Mot de passe</label>
        <input type="password" name="password"> </input>
        <br> <br>
        <label for="mail"> Mail</label>
        <input type="email" name="mail" value=  <?php if( isset( $_POST['mail']) && $_POST['mail']!='') {echo $_POST['mail'];}?>></input>
        <br> <br>
        <input type="submit" value="Valider">
        <br> <br>
        <br> <br>
        <?php

        if( isset( $_POST['pseudo']) && $_POST['pseudo']!='' &&  isset( $_POST['password']) && $_POST['password']!='' &&  isset( $_POST['mail']) && $_POST['mail']!='') 
        {
            $users = makeSelect('SELECT `pseudo` FROM `users`;'); // Selectionne le pseudo de chaque utlisateur

            $mails = makeSelect('SELECT `mail` FROM `users`;'); // Selectionne le mail de chaque utlisateur

            $resultPseudo = userExist($_POST, $users); // on verifie que le pseudo est disponible
 
            $resultMail = mailExist($_POST, $mails) ; // on verifie que le mail est disponible
            
            //on verifie que le format du mail est correct ensuite on verifie qu'il ne soit pas present dans la bdd
            
            if(filter_var($_POST['mail'],FILTER_VALIDATE_EMAIL) == false) { // verifie le bon format du mail
                echo '<span style="background-color:red;color:white;display:block;margin:10px 0;padding:4px 7px;">Le mail n\'est pas au bon format !</span>';

            } else {
                
                if($resultMail !== false) { // verifie si le mail est disponible
                    echo '<span style="background-color:red;color:white;display:block;margin:10px 0;padding:4px 7px;">Le mail est deja utilsé !</span>';
                } else {
                    if($resultPseudo !== false) { //verifie si l'utlisateur existe
                        echo '<span style="background-color:red;color:white;display:block;margin:10px 0;padding:4px 7px;">Le pseudo est deja utilisé !</span>';
                    } else {
                        makeStatement('INSERT INTO `users`(`pseudo`,`password`,`mail`) VALUES (:pseudo, :password , :mail);', $params);
                        echo '<span style="background-color:green;color:white;display:block;margin:10px 0;padding:4px 7px;">Votre compte a bien etait creé !</span>';

                   }
                }
            }
            
        } else 
        {

            if(isset( $_POST['pseudo']) || isset($_POST['password']) ||  isset( $_POST['mail']) ) 
            {

                echo '<span style="background-color:red;color:white;display:block;margin:10px 0;padding:4px 7px;">Vous devez saisir tous les champs !</span> ';
            }
        }

        ?>

    </form>
    <br> 
    <p> Pour vous connecter c'est <a href="index.php">ici</a></p>
    
</body>
</html>