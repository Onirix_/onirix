<?php

require_once('../ini.php');

/*
Note : c'est pas non plus des tests unitaires au sens propre du terme, mais c'est un début ;)
 */
if (!isset($_SESSION['id']) || $_SESSION['id']>3) {
    header('Location:../index.php');
    exit;
}       //t'es pas dev ? tu passe pas

$tests = array_slice(scandir('../unit_testing/'), 2);   //on va chercher tout ce qui est dans le dossier "unit_testing", sauf "." et ".." (grâce au array_slice)

foreach ($tests as $test) {                             //et on inclut tout ça
    include('../unit_testing/'.$test);
}
