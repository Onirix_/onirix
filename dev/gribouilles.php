<?php


require_once('../ini.php');

if (!isset($_SESSION['id']) || $_SESSION['id']>3) {
    header('Location:../index.php');
    exit;
}       //on dégage tout ce qui n'est pas du dev à poil dur


$pdo = new PDO(PDODSN, PDOUSERNAME, PDOPASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));      //règle soucis d'encodage, à voir s'il faut l'utiliser ailleurs

$str = file_get_contents('../doc/selectGribouilles.sql', 'r');

if ($sql = $pdo->prepare($str)) {
    if ($sql->execute()) {

        $output = '';

        while ($line = $sql->fetch(PDO::FETCH_ASSOC)) {
            $t=[];
            foreach ($line as $key => $value) {
                if ($key[0] === '@') {
                    $value = $value.'.png';
                }
                if ($key === '@fond') {
                    $f = strtolower(str_replace(" ", "", $value));
                } elseif ($key === 'type') {
                    $t[]='"'.$f.($value === 'sort' ? 'sort' : '').'"';
                    $t[]='"'.ucfirst($value).'"';
                } else {
                    $t[]='"'.$value.'"';
                }
            }
            $output .= implode(',', $t)."\n";
        }

        $ressource = fopen('./cardsref.csv', 'w');
        fwrite($ressource, iconv('UTF-8', 'Windows-1252', '"@image","name","description","cost","@pv","pf","@fond","type"'."\n".$output) );
        fclose($ressource);

        header('Location:./cardsref.csv');


        // unlink('./test.php');


    }
}
else {echo 'error !';}
