<?php


require_once('../ini.php');

if (!isset($_SESSION['id']) || $_SESSION['id']>3) {
    header('Location:../index.php');
    exit;
}       //on dégage tout ce qui n'est pas du dev à poil dur


$pdo = new PDO(PDODSN, PDOUSERNAME, PDOPASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));      //règle soucis d'encodage, à voir s'il faut l'utiliser ailleurs

$str = file_get_contents('../doc/sqlCreate.sql', 'r');


if ($pdo->query($str) !== false) {echo 'Base de donnée modifiée !<br><a href="../">Retour</a>';}
else {echo 'error !';}
