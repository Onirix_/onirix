<?php

require_once('ini.php');
require_once('common.php');

if(isset($_SESSION['role']) && ( $_SESSION['role'] === 1 || $_SESSION['role'] === 2)){

    if( !( isset( $_GET['id'] ) && is_numeric( $_GET['id'] ) ) ) // on verifie que l'id est numerique
    {
        header( 'Location:' . DOMAIN . 'admin.php' ); 
        exit;
    }
    else 
    
    {
        
        if( isset($_POST['id']) &&  isset($_POST['name']) && $_POST['name'] !== '' && isset($_POST['description']) && $_POST['description'] !== '' && isset($_POST['image']) && $_POST['image'] !== '' ) {
            makeStatement('UPDATE `cardsref` SET `id`=:p_id, `name`=:pname, `description`=:pdescription,`image`=:pimage WHERE `id`=:p_id', ['p_id' => $_POST['id'],'pname' => $_POST['name'], 'pdescription' => $_POST['description'],'pimage' => $_POST['image']]);   
        }

        $id =  $_GET['id']; // l'id recuperer de la page admin

        $perso = makeSelect('SELECT `types`.`name` AS "type_name", `factions`.`name` AS `faction_name`, `cardsref`.* FROM `cardsref`
                            INNER JOIN `types` ON `cardsref`.`type`= `types`.`id`
                            INNER JOIN `factions` ON `cardsref`.`id_faction`= `factions`.`id` WHERE `cardsref`.`id`=:p_id', ['p_id' => $_GET['id']]); // on selectionne le personnage grace à l'id (quand il y a des inner join il faut specifier la table visée dans le where )

       
        ?>

        <!-- Formulaire pour modifier un personnage -->

        <h2> Modifier la fiche du personnage : <?php foreach ($perso as $value) { echo $value['name'];}?></h2>
        <form action="" method="POST" >
            <table cellpadding="10px" border="1px" style="border-collapse: collapse">
                <thead>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Cout</th>
                    <th>Points de Vie </th>
                    <th>Points de Force</th>
                    <th>Sort</th>
                    <th>Faction</th>
                    <th> Image</th>
                    
                </thead>
                <tbody>
                    <?php foreach ($perso as $value) { ?>
                    
                    <tr>
                        <td><?php echo $value['id']?> <input type="hidden" name="id" value="<?php echo $value['id']?>"></td>
                        <td><input type="text" name="name" placeholder="" value="<?php echo $value['name']?>"></td>
                        <td><textarea name="description" cols="20" rows="3" ><?php echo $value['description']?></textarea></td>
                        <td><?php echo $value['type_name']?></td>
                        <td><?php echo $value['cost']?></td>
                        <td><?php echo $value['pv']?></td>
                        <td><?php echo $value['pf']?></td>
                        <td><?php echo $value['spell']?></td>
                        <td><?php echo $value['faction_name']?></td>
                        <td><input type="text" name="image" placeholder="" value="<?php echo $value['image']?>"></td>
                        
                    </tr>

                    <?php } ?>
                </tbody>           
            </table>
            <br>
            <input type="submit" value="Valider">
        </form>

        <p><a href="admin.php">Retour à l'accueil</a></p>

  <?php 

    }



} 
else 
{

     header('Location:./game.php'); 
}

