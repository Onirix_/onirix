<?php
ini_set("display_errors", 1);       //active l'affichage des erreurs
session_start();
session_destroy();

foreach ($_SESSION as $key => $value) {
    unset($_SESSION[$key]);
}

header('Location:./index.php');
