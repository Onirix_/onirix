
<?php
/**
 * Creer une instance de PDO
 * @return string instance de PDO
 */
function getPdo()
{
    static $pdo = null; //static : $pdo est partagé par tout les appels de getPdo()

    if(is_null($pdo))
    {
        try
        {
            $pdo = new PDO('mysql:host=localhost;dbname=objectif_onirix;charset=utf8', 'root', '');
        }
        catch(PDOException $e)
        {
            die($e);
        }
    }

    return $pdo;
}

/**
 * Effectue une requete SQL
 * @param  string $sql    Votre requete
 * @param  array $params Un tableau associatif de la forme ['placeholder' => valeur]
 * @return PDOStatement         le retour de votre requete
 */
function makeStatement($sql, $params = [])
{
    $pdo = getPdo();
    if(empty($params))
    {
        return $pdo->query($sql);
    }
    else
    {
        $statement = $pdo->prepare($sql);
        foreach ($params as $key => $value)
        {
            $statement->bindValue($key, $value);
        }
        $statement->execute();

        return $statement;
    }
}


function makeSelect($sql, $params = [], $fetchStyle = PDO::FETCH_ASSOC, $fetchArg = null)
{
    $statement = makeStatement($sql, $params);

    if(is_null($fetchArg))
    {
        $result = $statement->fetchAll($fetchStyle);
    }
    else
    {
        $result = $statement->fetchAll($fetchStyle, $fetchArg);
    }
    $statement->closeCursor();

    return $result;
}
/**
 * Verifie l'existence d'un utilisateur dans la bdd
 * @param  string $needle    aiguille
 * @param  array $haystack  botte de foin
 * @return mixed true or false
 */
function userExist( $needle, $haystack ) {
    foreach( $haystack as $item ) : // Pour chaque utilisateur du tableau,
        if( $needle['pseudo']==$item['pseudo']) {
            return true; // Si le pseudo existe on retourne true
        }

    endforeach;

    return false;
}

/**
 * Verifie l'existence d'un mail dans la bdd
 * @param  string $needle    aiguille
 * @param  array $haystack  botte de foin
 * @return mixed true or false
 */

function mailExist( $needle, $haystack ) {
    foreach( $haystack as $item ) : // Pour chaque utilisateur du tableau,
        if( $needle['mail']==$item['mail']) {
            return true; // Si le mail existe on retourne true
         }
    endforeach;

    return false;
}