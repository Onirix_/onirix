
            <div class="container game">

                <?php
                if ($o->getGame()->checkEndGame()) {
                    echo '<div id="gameOver"><h1>C\'EST FINI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</h1>
                    <a href="./controllers/endGame.php">Quitter la partie</a></div>';
                }
                ?>


                <!-- Partie du second joueur -->
                <div class="row ">

                    <div class="col-md-1 icones">
                        <div class="row">
                            <div class="col-lg-12" id="pv_j2">
                                <img src="./img/icone_coeur.png" class="icones_game" alt="">
                            </div>

                            <p class="col-md-12" id="pv_j2"><?= $o->getOpponent()->getPV(); ?></p>
                        </div>

                    </div>
                    <div class="col-md-1 icones">
                        <img src="./img/icone_mana.png" class="icones_game" alt="">
                        <p id="pm_j2">
                            <?= $o->getOpponent()->getPM() ?>
                        </p>
                    </div>
                    <div class="col-md-8 cartesJ1">
                        <?php
                            for ($i=0; $i < $o->getOpponent()->getHandSize(); $i++) {
                                echo '<img src="./img/dos_cartes.png" class="dos_cartes" alt="">';
                            }
                        ?>
                    </div>
                    <div class="col-md-2">
                            <?php
                            if ( $o->getOpponent()->getDeckSize() > 0) {
                                ?>
                                <div class="draw_j2">
                            <img src="./img/pioche.png" class="piochej2" alt="">

                        </div>
                        <p><?= $o->getOpponent()->getDeckSize(); ?></p>
                            <?php
                            }
                            ?>
                        <!--  Défausse ?-->
                    </div>

                </div>

                <!-- if (isset($_GET['attacking'])) {
                    echo '<a href="./gameControllers/targetCard.php?card='.$_GET['attacking'].'&target=0">'.$o->getOpponent()->getPseudo().'</a><br>';
                } else {
                    echo $o->getOpponent()->getPseudo().'<br>';
                } -->

                <!-- Les cartes (en attente) jouées par le second joueur -->
                <div class="row ">

                    <div class="col-md-2">
                        <div id="avatar_j2" class="avatar" style="transform: rotate(180deg);">
                            <?php
                            if (isset($_GET['attacking'])) {
                                echo '<a href="./gameControllers/targetCard.php?card='.$_GET['attacking'].'&target=0">';
                            }
                            ?>
                            hero
                            <?php
                            if (isset($_GET['attacking'])) {
                                echo '</a>';
                            }
                            ?>
                        </div>
                    </div>


                    <div class="col-md-4 offset-md-3 ">
                        <div id="await_j2" class="row await">

                            <?php  foreach ($o->getOpponent()->getRest() as  $card) {

                                    echo '<div id="card_' . $card->getId() . '" class="col-md-1 awaitCard">';
                                    if (isset($_GET['attacking'])) {
                                        echo '<a href="./gameControllers/targetCard.php?card='.$_GET['attacking'].'&target='.$card->getId().'"> ' .
                                        '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt="" title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">' .
                                        '</a>';
                                    } else {
                                        echo '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt="" title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">';
                                    }
                                    echo '</div>';
                                } ;
                            ?>

                        </div>

                    </div>
                    <div class="col-md-2">
                        <p id="discard_J2">Discard : <?= $o->getOpponent()->getDiscardSize(); ?></p>
                    </div>

                </div>

                <!-- Les cartes jouées par le second joueur -->
                <div class="row carte_en_jeu">

                    <div class="col-md-6 offset-md-3 ">
                        <div class="row">
                                <?php foreach ($o->getOpponent()->getBoard()  as $card) {
                                    echo '<div id="card_' . $card->getId() . '" class="col-md-1 boardCard">';
                                    if (isset($_GET['attacking'])) {
                                        echo '<a href="./gameControllers/targetCard.php?card='.$_GET['attacking'].'&target='.$card->getId().'"> ' .
                                        '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt=""  title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">' .
                                        '</a>';
                                    } else {
                                        echo '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt=""  title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">';
                                    }
                                    echo '</div>';
                                }
                                ?>
                        </div>

                    </div>
                    <div class="col-md-2">

                    </div>

                </div>

                <!-- Les cartes jouées par le premier joueur -->
                <div class="row carte_en_jeu ">

                    <div class="col-md-6 offset-md-3 ">
                        <div class="row ">

                                <?php foreach ($o->getMyself()->getBoard()  as $card ) {
                                    echo '<div id="card_' . $card->getId() . '" class="col-md-1 boardCard">';
                                    if (!isset($_GET['attacking']) && $o->getGame()->amIActivePlayer()) {
                                        echo '<a href="./gameControllers/attackCard.php?card='.$card->getId().'"> ' .
                                        '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt=""  title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">' .
                                        '</a> ';
                                    } elseif (isset($_GET['attacking']) && +$_GET['attacking'] === $card->getId() ) {
                                        echo '<a href="?"><img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures attacking" alt=""  title="pf : '.$card->getPF().' / pv : '.$card->getPV().'"></a>';
                                    } else {
                                        echo '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt=""  title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">';
                                    }
                                    echo '</div>';
                                }
                                ?>

                        </div>

                    </div>
                    <div class="col-md-2">
                    </div>

                </div>


                <!-- Les cartes (en attente) jouées par le premier joueur -->
                <div class="row await">

                    <div class="col-md-2">
                        <div id="avatar_j2" class="avatar">
                            hero
                        </div>
                    </div>

                    <div class="col-md-4 offset-md-3 ">
                        <div class="row await">

                            <?php  foreach ($o->getMyself()->getRest() as  $card) {
                                    echo '<div id="card_' . $card->getId() . '" class="col-md-1 boardCard">' .
                                    '<img src="./img/miniature_cartes/' . $card->getImage() . '.png" class="miniatures" alt=""  title="pf : '.$card->getPF().' / pv : '.$card->getPV().'">' .
                                    '</div>';
                                } ;
                            ?>

                        </div>

                    </div>
                    <div class="col-md-2">
                        <p id="discard_J1">Discard : <?= $o->getMyself()->getDiscardSize(); ?></p>
                    </div>

                </div>



                <!-- Partie du premier joueur -->
                <div class="row first_player">
                    <div class="col-md-1 iconesj1">
                        <div class="row">
                            <div class="col-lg-12">
                                <img src="./img/icone_coeur.png" class="icones_game" alt="">
                            </div>

                            <p class="col-md-12"><?= $o->getMyself()->getPV() ?></p>
                        </div>

                    </div>
                    <div class="col-md-1 iconesj1">
                        <img src="./img/icone_mana.png" class="icones_game" alt="">
                        <p><?= $o->getMyself()->getPM() ?></p>
                    </div>
                    <div class="col-md-8 cartesJ1">
                                            <?php
                        foreach ($o->getMyself()->getHand()  as $card ) {
                            if ($o->getGame()->amIActivePlayer()) {
                                echo '<a href="./gameControllers/playCard.php?card='.$card->getId().'"><img src="./img/cartes/' . $card->getImage() . '.jpg" class="cartes" alt=""></a>';
                            } else {
                                echo '<img src="./img/cartes/' . $card->getImage() . '.jpg" class="cartes" alt="">';
                            }
                        }
                        ?>


                    </div>
                    <div class="col-md-2">
                        <?php
                        if ( $o->getMyself()->getDeckSize() > 0) {
                            ?>
                        <div class="pioche_j1">
                            <img src="./img/pioche.png" class="piochej1" alt="">
                            <p><?= $o->getMyself()->getDeckSize(); ?></p>
                        </div>
                            <?php
                        }
                        ?>

                    </div>

                </div>

            </div>
