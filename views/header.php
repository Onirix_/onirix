<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="./css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/style.min.css">
        <script type="text/javascript">
            document.addEventListener( 'keydown', function( e ){
                if ( e.keyCode === 32  ) {
                    window.location.replace('./gameControllers/pass.php');
                }
            } )
        </script>
        <title>Onirix</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="container header">
                <div class="row">
                    <div class="col-md-2">
                        <img src="./img/logo_onirix.png" class="logo_onirix" alt="">
                    </div>
                    <div class="col-md-2 offset-md-2">
                        <a href="./controllers/deconnect.php">Déconnexion </a>
                        <?php
                        if ($o->getGame()->amIActivePlayer()) {
                            echo '/ <a href="./gameControllers/pass.php">Pass</a>';
                        }
                        ?>
                    <br>
                    Turn : <?= $o->getGame()->getTurn(); ?>
                    </div>
                </div>
            </div>
