<?php

require_once('./ini.php');
if (!isset($_SESSION['id'])) {                //si on n'est pas connecté, on affiche le formulaire de connection
?>

<h1>Connexion à Onirix</h1>

<form action="./controllers/connect.php" method="POST">
    <input type="text" name="pseudo" placeholder="pseudo">
    <input type="password" name="password" placeholder="password">
    <input type="submit" value="Valider">
</form>


<p> Si vous ne possedez pas de compte, creer un compte <a href="createuser.php">ici</a></p>
<?php
} else {
    header('Location:./game.php');          //si on est connecté, on redirige vers la partie
}
