<?php
require_once('./ini.php');
require_once(CLASSESPATH.'matchMaking.class.php');


$pageTitle = 'Game';
if ( isset( $_SESSION['id'] ) ) {
    switch ( MatchMaking::getPlayerState() ) {
        case 1:
            // Partie en attente d'adversaire
            echo 'En attente d\'adversaire, veillez patienter'."<br>\n";
            echo '(vous jouez '.MatchMaking::getPlayerFaction().')';
            break;
        case 2:
            require_once( CONTROLLERSPATH.'gameLoad.php' );
            // include_once('./views/board.php');
            // Partie en cours : Jouer
            break;
        default:
            $a = MatchMaking::findOpponent();
            if ($a !== []) {
                $opponentId = $a['opponentId'];
                $advFactionId = $a['advFactionId'];
                require_once( CONTROLLERSPATH.'gameCreate.php' );
            } else {
                require_once( './views/chooseFaction.php' );
            }
            break;
    }
} else {
    header( 'Location:./index.php' );
}
